function search(event) {
    let address = event.target.value; 
    fetch("https://api-adresse.data.gouv.fr/search/?q=" + address).then(response => {
        if (!response.ok) {
            console.warn(`Erreur ${response.status}`);
        }

        return response.json()
    })
    .then(data => {

        // Remove nodes
        const myNode = document.getElementById('addresse-previsualisation');
        while(myNode.firstChild) {
            myNode.removeChild(myNode.lastChild);
        }

        data.features.forEach(element => {
            let div = document.createElement('div');
            div.innerHTML = element.properties.label;
            div.classList.add('addresse');
            myNode.appendChild(div);
        });
    })
    .catch(error => {
        console.warn(error);
    })
}